@extends('layout.front-master')
@section('title', 'Lance Master | Search Results')

@section('content')

    <div class="videos-row row justify-content-md-center">
        <div class="col-lg-12" style="margin-top: 60px;">
            <div class="filters">
               
                <div class="row filter-row">
                    @foreach($categories as $category)
                   
                     <div class="col-md-2 pl-2 pr-1 topic-box" id="categories-{{$category->id}}" >
                        <span class="gcontent-box category-content"  id="category-content-{{$category->id}}" onclick="retrieveCategory(this.id)" data-category="{{ $category->id }}">
                                {{ $category->name }}
                        </span>
                    </div>
                    @endforeach

                @foreach($topics as $topic)
                   
                   <div class="col-md-2 pl-2 pr-1 topic-box" id="topics">
                      <span class="gcontent-box topic-content" id="topic-content-{{ $topic->id }}" onclick="retrieveTopic(this.id)"  data-topic="{{ $topic->id }}">
                              {{ $topic->name }}
                      </span>
                  </div>
                  @endforeach

                        
                        <!-- <select name="category_id" class="form-control" id="categories">
                            <option value="">Select Category</option>
                            @foreach($categories as $category)
                                <option value="{{ $category->id }}" @if($category->id == $category_id) selected @endif>{{ $category->name }}</option>
                            @endforeach
                        </select> -->
                  
                    <!-- <div class="col-md-6 pl-2 pr-1">
                        <select name="topic_id" class="form-control" id="topics">
                            <option value="">Select Topic</option>
                            @foreach($topics as $topic)
                                <option value="{{ $topic->id }}" @if($topic->id == $topic_id) selected @endif>{{ $topic->name }}</option>
                            @endforeach
                        </select>
                    </div> -->
                </div>
        
            
            <div class="heading">
                <h2>Search Results</h2>
            </div>
            <div class="slick-slider">
                @foreach ($searchVideos as $video)
                    <div>
                        <div class="boxImg">
                            @php
                                if($video->continueWatches->first()){
                                    $v_time = round($video->continueWatches->first()->time);
                                }
                                else{
                                    $v_time = 0;
                                }
                            @endphp
                            <img src="{{ asset($video->thumbnail) }}" data-href="{{ URL::to('/video', $video->id) }}"
                                 class="video-list clickable" />
                            {{-- <video controls width='100%' id="recommendedVideoPlayer{{ $video->id }}" height='200px' onclick="playVideo(this.id);">
                                <source src="{{ asset($video->video_path) }}">
                            </video> --}}
                            <div class="pt-3">
                                <div class="details mt-1">
                                    <div class="profile-pic">
                                        <img src="{{ asset('assets/front/images/dummy.jpg') }}" alt="">
                                    </div>
                                    <div class="video-details">
                                        <div>{{ $video->title }}</div>
                                        <div class="channel">
                                            <a href="{{ route('channel.index', $video->user->id) }}" class="color-white">
                                                <span class="text-capitalize">{{ $video->user->name }}</span>
                                            </a>
                                        </div>
                                    </div>
                                    <p class="ml-auto">
                                        @if (isset($video->views))
                                        {{ sizeof($video->views) }} Views
                                        @endif
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>

    <div class="videos-row row justify-content-md-center">
        <div class="col-lg-12">
            <div class="heading">
                <h2>Previously Watched</h2>
            </div>
            <div class="slick-slider">
                @foreach ($watchlistVideos as $video)
                    <div>
                        <div class="boxImg">
                            @php
                                if($video->continueWatches->first()){
                                    $v_time = round($video->continueWatches->first()->time);
                                }
                                else{
                                    $v_time = 0;
                                }
                            @endphp
                            <img src="{{ asset($video->thumbnail) }}" data-href="{{ URL::to('/video', $video->id) }}"
                                class="video-list clickable" />
                            {{-- <video controls width='100%' id="recommendedVideoPlayer{{ $video->id }}" height='200px' onclick="playVideo(this.id);">
                                <source src="{{ asset($video->video_path) }}">
                            </video> --}}
                            <div class="pt-3">
                                <div class="details mt-1">
                                    <div class="profile-pic">
                                        <img src="{{ asset('assets/front/images/dummy.jpg') }}" alt="">
                                    </div>
                                    <div class="video-details">
                                        <div>{{ $video->title }}</div>
                                        <div class="channel">
                                            <a href="{{ route('channel.index', $video->user->id) }}" class="color-white">
                                                <span class="text-capitalize">{{ $video->user->name }}</span>
                                            </a>
                                        </div>
                                    </div>
                                    <p class="ml-auto">
                                        @if (isset($video->views))
                                        {{ sizeof($video->views) }} Views
                                        @endif
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>

    <div class="videos-row row justify-content-md-center">
        <div class="col-lg-12">
            <div class="heading">
                <h2>People Also Watched</h2>
            </div>
            <div class="slick-slider">
            @foreach ($contWatchesVideos as $video)
                <div>
                    <div class="boxImg">
                        @php 
                            if($video->continueWatches->first()){
                                $v_time = round($video->continueWatches->first()->time);
                            }
                            else{
                                $v_time = 0;
                            }
                        @endphp
                        <img src="{{ asset($video->thumbnail) }}" data-href="{{ URL::to('/video', $video->id) }}"
                            class="video-list clickable" />
                        {{-- <video controls width='100%' id="recommendedVideoPlayer{{ $video->id }}" height='200px' onclick="playVideo(this.id);">
                            <source src="{{ asset($video->video_path) }}">
                        </video> --}}
                        <div class="pt-3">
                            <div class="details mt-1">
                                <div class="profile-pic">
                                    <img src="{{ asset('assets/front/images/dummy.jpg') }}" alt="">
                                </div>
                                <div class="video-details">
                                    <div>{{ $video->title }}</div>
                                    <div class="channel">
                                        <a href="{{ route('channel.index', $video->user->id) }}" class="color-white">
                                            <span class="text-capitalize">{{ $video->user->name }}</span>
                                        </a>
                                    </div>
                                </div>
                                <p class="ml-auto">
                                    @if (isset($video->views))
                                    {{ sizeof($video->views) }} Views
                                    @endif
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
        </div>
    </div>

    <div class="videos-row row justify-content-md-center">
        <div class="col-lg-12">
            <div class="heading"   style="text-de: 2px solid white">
                <h2>Latest From Official Channels</h2>
            </div>
            <div class="slick-slider" >
            @foreach ($trendingVideos as $video)
                <div>
                    <div class="boxImg">
                        @php 
                            if($video->record->continueWatches->first()){
                                $v_time = round($video->record->continueWatches->first()->time);
                            }
                            else{
                                $v_time = 0;
                            }
                        @endphp
                        <img src="{{ asset($video->record->thumbnail) }}" data-href="{{ URL::to('/video', $video->record->id) }}"
                            class="video-list clickable" />
                        {{-- <video controls width='100%' id="recommendedVideoPlayer{{ $video->record->id }}" height='200px' onclick="playVideo(this.id);">
                            <source src="{{ asset($video->record->video_path) }}">
                        </video> --}}
                        <div class="pt-3">
                            <div class="details mt-1">
                                <div class="profile-pic">
                                    <img src="{{ asset('assets/front/images/dummy.jpg') }}" alt="">
                                </div>
                                <div class="video-details">
                                    <div>{{ $video->record->title }}</div>
                                    <div class="channel">
                                        <a href="{{ route('channel.index', $video->record->user->id) }}" class="color-white">
                                            <span class="text-capitalize">{{ $video->record->user->name }}</span>
                                        </a>
                                    </div>
                                </div>
                                <p class="ml-auto">
                                    @if (isset($video->record->views))
                                    {{ sizeof($video->record->views) }} Views
                                    @endif
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
        </div>
    </div>

@endsection

@push('scripts')
    <script>

    function retrieveCategory(id){
        const category_id = id.match(/(\d+)/);
        
        window.location.href = 'search?category_id=' + category_id[0];
    }
    function retrieveTopic(id){
        const topic_id = id.match(/(\d+)/);

        //topic is the same as category id

        window.location.href = 'search?category_id=' + topic_id[0] + '&topic_id=' + topic_id[0];
    }
        // $.ajajxSetup({
        //     headers: {
        //         'X-CSRF-TOKEN' : $('meta[name="csrf-token"]').attr('content')
        //     }
        // });
    $( function() {
        $('.slick-track').addClass('float-left');

        // $('#categories').on('change', function() {
        //     window.location.href = 'search?category_id=' + $(this).find(':selected').val();
        // });

        $('#topics').on('click', function() {
            window.location.href = 'search?category_id=' + $('.category-content').data().category + '&topic_id=' + $('.topic-content').data().topic;
        });

        // $('#categories').on('click',function(){
           
        //     window.location.href = 'search?category_id=' + $('.category-content').data().category;
        //     // $.ajax({
        //     //     url: '',
        //     //     method: '',
        //     //     success: function(){

        //     //     }
        //     // })
        // });
    });
    </script>
@endpush
